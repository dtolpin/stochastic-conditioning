all: build

NITER=10000
GO=go

build: check-python-version get-py check-go-version get-infergo build-commute build-nypopu build-sailing

# Check Python and install necessary modules
check-python-version:
	which python || (echo ERROR: python not installed && exit 1)
	which pip || (echo ERROR: pip not installed && exit 1)
	(python -V 2>&1 | grep 'Python 3.[56789]' > /dev/null && echo `python -V 2>&1` is supported) || \
		(echo ERROR: unsupported `python -V 2>&1` && exit 1)
get-py:
	pip install -q jupyter notebook numpy scipy matplotlib 

# Check Go and install Infergo
check-go-version:
	which $(GO) || (echo ERROR: go not installed && exit 1)
	($(GO) version | grep 'go1.1[2-6]' > /dev/null && echo `$(GO) version` is supported) || \
		(echo ERROR: unsupported `$(GO) version` && exit 1)
get-infergo: 
	$(GO) get -u bitbucket.org/dtolpin/infergo/cmd/deriv

build-%:
	(cd `echo $@| sed 's/build-//'` && make build GO=$(GO))

run: build run-commute run-nypopu run-sailing
run-%: build-%
	(cd `echo $@| sed 's/run-//'` && make run GO=$(GO) NITER=$(NITER))

clean: clean-commute clean-nypopu clean-sailing
clean-%:
	(cd `echo $@| sed 's/clean-//'` && make clean GO=$(GO))

pack: build
	(cd .. && zip stochastic-conditioning/studies.zip \
		stochastic-conditioning/README.md \
		stochastic-conditioning/Makefile \
		stochastic-conditioning/go.mod \
		stochastic-conditioning/go.sum \
		stochastic-conditioning/commute/README.md \
		stochastic-conditioning/commute/Makefile \
		stochastic-conditioning/commute/main.go \
		stochastic-conditioning/commute/simu/simu.go \
		stochastic-conditioning/commute/cmd/simulate/main.go \
		stochastic-conditioning/commute/problem/problem.go \
		stochastic-conditioning/commute/model/model.go \
		stochastic-conditioning/commute/model/ad/model.go \
		stochastic-conditioning/commute/posterior.ipynb \
		stochastic-conditioning/commute/data-30.csv \
		stochastic-conditioning/commute/posterior-30-d.csv \
		stochastic-conditioning/commute/posterior-30-a.csv \
		stochastic-conditioning/commute/posterior-30-s.csv \
		stochastic-conditioning/commute/posterior-30-i.csv \
		stochastic-conditioning/nypopu/README.md \
		stochastic-conditioning/nypopu/Makefile \
		stochastic-conditioning/nypopu/main.go \
		stochastic-conditioning/nypopu/model/model.go \
		stochastic-conditioning/nypopu/model/ad/model.go \
		stochastic-conditioning/nypopu/posterior.ipynb \
		stochastic-conditioning/nypopu/sample-1.csv \
		stochastic-conditioning/nypopu/sample-2.csv \
		stochastic-conditioning/nypopu/total.csv \
		stochastic-conditioning/nypopu/posterior-1.csv \
		stochastic-conditioning/nypopu/posterior-2.csv \
		stochastic-conditioning/sailing \
		stochastic-conditioning/sailing/README.md \
		stochastic-conditioning/sailing/Makefile \
		stochastic-conditioning/sailing/main.go \
		stochastic-conditioning/sailing/domain.go \
		stochastic-conditioning/sailing/domain_test.go \
		stochastic-conditioning/sailing/bellman.go \
		stochastic-conditioning/sailing/posterior.ipynb \
		stochastic-conditioning/sailing/results/samples-25-0.csv \
		stochastic-conditioning/sailing/results/samples-25-1.csv \
		stochastic-conditioning/sailing/results/samples-25-2.csv \
		stochastic-conditioning/sailing/results/samples-25-3.csv \
		stochastic-conditioning/sailing/results/samples-25-4.csv \
		stochastic-conditioning/sailing/results/samples-25-5.csv \
		stochastic-conditioning/sailing/results/samples-50-0.csv \
		stochastic-conditioning/sailing/results/samples-50-1.csv \
		stochastic-conditioning/sailing/results/samples-50-2.csv \
		stochastic-conditioning/sailing/results/samples-50-3.csv \
		stochastic-conditioning/sailing/results/samples-50-4.csv \
		stochastic-conditioning/sailing/results/samples-50-5.csv \
		stochastic-conditioning/sailing/results/samples-100-0.csv \
		stochastic-conditioning/sailing/results/samples-100-1.csv \
		stochastic-conditioning/sailing/results/samples-100-2.csv \
		stochastic-conditioning/sailing/results/samples-100-3.csv \
		stochastic-conditioning/sailing/results/samples-100-4.csv \
		stochastic-conditioning/sailing/results/samples-100-5.csv \
		stochastic-conditioning/sailing/results/samples-200-3.csv \
		stochastic-conditioning/sailing/results/samples-200-4.csv \
		stochastic-conditioning/sailing/results/samples-200-5.csv \
		stochastic-conditioning/sailing/results/summary-25-0.txt \
		stochastic-conditioning/sailing/results/summary-25-1.txt \
		stochastic-conditioning/sailing/results/summary-25-2.txt \
		stochastic-conditioning/sailing/results/summary-25-3.txt \
		stochastic-conditioning/sailing/results/summary-25-4.txt \
		stochastic-conditioning/sailing/results/summary-25-5.txt \
		stochastic-conditioning/sailing/results/summary-50-0.txt \
		stochastic-conditioning/sailing/results/summary-50-1.txt \
		stochastic-conditioning/sailing/results/summary-50-2.txt \
		stochastic-conditioning/sailing/results/summary-50-3.txt \
		stochastic-conditioning/sailing/results/summary-50-4.txt \
		stochastic-conditioning/sailing/results/summary-50-5.txt \
		stochastic-conditioning/sailing/results/summary-100-0.txt \
		stochastic-conditioning/sailing/results/summary-100-1.txt \
		stochastic-conditioning/sailing/results/summary-100-2.txt \
		stochastic-conditioning/sailing/results/summary-100-3.txt \
		stochastic-conditioning/sailing/results/summary-100-4.txt \
		stochastic-conditioning/sailing/results/summary-100-5.txt \
		stochastic-conditioning/model/model.go \
		stochastic-conditioning/infer/pmmh.go )
