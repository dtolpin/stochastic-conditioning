# Case studies of stochastic conditioning

This folder contains three case studies accompanying the article
"Probabilistic programs with stochastic conditioning".

1. `commute/` — Inferring the accuracy of weather forecast.
2. `nypopu/` — Estimating the population of New York state.
3. `sailing/` — The sailing problem.

The studies are implemented in [Go](http://golang.org/) using
[Infergo](http://infergo.org/). A Go version 1.12 or newer is
required. At the time of writing, the current Go version is 1.15,
and 1.16 release candidate is supported. Posterior analysis is
performed in Python 3 Jupyter notebooks.

To run all experiments, execute 'make run' in the root
directory. To run experiments for a particular case study, run
'make run' in the study's folder.  As provided, the study
folders include inference results: samples from the posterior
and summaries. Running 'make run' will re-generate the samples,
and the run may take a few minutes, depending on the hardware
configuration used.

Each folder contains notebook `posterior.ipynb`. The posterior
distributions are visualized and analyzed in these notebooks.
