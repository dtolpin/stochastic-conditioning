package main

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/mathx"
	"flag"
	"fmt"
	"log"
	"math"
	"math/rand"
	. "stochastic-conditioning/infer"
	"time"
)

func init() {
	// randomize executions
	rand.Seed(time.Now().UnixNano())
}

// Inference

// Myopic policy selects a leg with the shortest
// cost-to-goal estimate. The heuristic is
// cost-to-goal ≈ leg-cost + unit-cost*dist-to-goal
// unit-cost affects the balance between cheap
// and good moves. Eventually, we want to learn its posterior
// distribution.
type myopic struct {
	uCost float64
}

func (pol *myopic) leg(size int, p pos, t, w int) (l int) {
	g := pos{float64(size) - 1, float64(size) - 1}
	lbest := -1
	cbest := math.Inf(1)
	q := pos{}
legs:
	for l := 0; l != nDir; l++ {
		if cprod(dirs[l], dirs[w]) == -1 {
			// cannot go into wind
			continue legs
		}

		for i := 0; i != 2; i++ {
			q[i] = p[i] + dirs[l][i]
			if int(q[i]) == -1 || int(q[i]) == size {
				// cannot direct into shore
				continue legs
			}
		}

		c := legCost(t, l, w) + dist(q, g)*pol.uCost
		if c < cbest {
			cbest = c
			lbest = l
		}
	}
	return lbest
}

// ParticleModel

// we transform unbounded x to the range of possible values of uCost
func xToUCost(x float64) float64 {
	minCost := awayCost
	maxCost := delayCost + (awayCost+2*downCost+2*crossCost+2*upCost)/7
	return minCost + (maxCost-minCost)*mathx.Sigm(x)
}

type model struct {
	size    int
	logTemp float64
}

func (m *model) String() string {
	return fmt.Sprintf("{Size:%v LogTemp:%v}", m.size, m.logTemp)
}

func (m *model) NewParticle() interface{} {
	return &winds{}
}

func (m *model) Observe(x []float64, p interface{}) (logp float64) {
	logp += Normal.Logp(0, 1.6, x[0]) // combined with Sigm ≈ uniform
	uCost := xToUCost(x[0])
	winds := p.(*winds)
	c, _ := traverse(m.size, &myopic{uCost}, winds)
	logp -= c / float64(m.size) * math.Exp(-m.logTemp)
	return logp
}

// nested model
type nestedModel struct {
	model
	miter int
}

// Proposal distribution, for either model
func q(x, xn []float64) {
	xn[0] = x[0] + SIGMA*rand.NormFloat64()
}

func (m *nestedModel) Observe(x []float64) (logp float64) {
	logp += Normal.Logp(0, 1.6, x[0]) // combined with Sigm ≈ uniform
	uCost := xToUCost(x[0])
	c := 0.
	for i := 0; i != m.miter; i++ {
		ci, _ := traverse(m.size, &myopic{uCost}, &winds{})
		c += ci
	}
	c /= float64(m.miter)
	logp -= c / float64(m.size) * math.Exp(-m.logTemp)
	return logp
}

// Command line

func init() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`Infers average unit cost in the sailing domain:
		sailing [OPTIONS]`+"\n")
		flag.PrintDefaults()
	}
	flag.IntVar(&SIZE, "size", SIZE, "lake size")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&MITER, "miter", MITER, "number of inner iterations")
	flag.IntVar(&THIN, "thin", THIN, "output 1 of each <thin> samples")
	flag.IntVar(&NBURN, "nburn", NBURN, "number of burned iterations")
	flag.IntVar(&NPAR, "npar", NPAR, "number of particles")
	flag.Float64Var(&SIGMA, "sigma", SIGMA, "proposal standard deviation")
	flag.Float64Var(&LOGTEMP, "logtemp", LOGTEMP, "log temperature")
	flag.Float64Var(&EPS, "eps", EPS, "precision of state cost")
	flag.BoolVar(&NESTED, "nested",  NESTED, "nested model")

	log.SetFlags(0)
}

// Option defaults
var (
	SIZE    = 25
	SIGMA   = 0.5
	NITER   = 10000
	MITER   = 0
	THIN    = 1
	NBURN   = 0
	NPAR    = 20
	LOGTEMP = 0.
	EPS     = 0.01
	NESTED  = false
)

func main() {
	flag.Parse()
	if NBURN == 0 {
		NBURN = NITER
	}
	if MITER == 0 {
		for  MITER*MITER < NITER {
			MITER++
		}
	}

	if flag.NArg() != 0 {
		log.Fatalf("unexpected: positional arguments: %v",
			flag.Args())
	}

	var (
		sm *model
		nm *nestedModel
	)
	if NESTED {
		nm  = &nestedModel{miter: MITER}
		sm = (*model)(&nm.model)
	} else {
		sm = &model{}
	}
	sm.size = SIZE
	sm.logTemp = LOGTEMP

	// Inference
	log.Printf("Model: %v", sm)

	x := []float64{rand.Float64()}
	for i := range x {
		x[i] = 0.1 * rand.NormFloat64()
	}
	samples := make(chan []float64)
	var (
		sampler *infer.Sampler
		stop func()
	)
	if NESTED {
		mh := &MH{
			Q: q,
		}
		sampler = &mh.Sampler
		stop = mh.Stop
		mh.Sample(nm, x, samples)
	} else {
		mh := &PMMH{
			NPar: NPAR,
			Q: q,
		}
		sampler = &mh.Sampler
		stop = mh.Stop
		mh.Sample(sm, x, samples)
	} 

	// Burn
	for i := 0; i != NBURN; i++ {
		<-samples
	}

	meanUCost, meanCost, n := 0., 0., 0.
	for i := 0; i != NITER; i++ {
		x = <-samples
		if len(x) == 0 {
			break
		}
		uCost := xToUCost(x[0])
		meanUCost += uCost
		// Re-run the simulator with the unit cost sample
		cost, _ := traverse(SIZE, &myopic{uCost}, &winds{})
		meanCost += cost
		n++
		if (i+1)%THIN == 0 {
			fmt.Printf("%v,%v\n", uCost, cost)
		}
	}
	stop()
	meanUCost /= n
	meanCost /= n
	log.Printf("Mean unit cost: %.3f", meanUCost)
	log.Printf("Mean travel cost: %.3f", meanCost)
	log.Printf("MH acceptance rate: %.3f",
		float64(sampler.NAcc)/float64(sampler.NAcc+sampler.NRej))

	// Compute some baselines
	optimisticMeanCost, n := 0., 0.
	for i := 0; i != NITER; i++ {
		cost, _ := traverse(SIZE, &myopic{awayCost}, &winds{})
		optimisticMeanCost += cost
		n++
	}
	optimisticMeanCost /= n
	log.Printf("Mean optimistic travel cost: %.3f", optimisticMeanCost)

	greedyMeanCost, n := 0., 0.
	for i := 0; i != NITER; i++ {
		cost, _ := traverse(SIZE, &myopic{delayCost + upCost}, &winds{})
		greedyMeanCost += cost
		n++
	}
	greedyMeanCost /= n
	log.Printf("Mean greedy travel cost: %.3f", greedyMeanCost)

	optimalMeanCost := bellman(SIZE, EPS)
	log.Printf("Mean optimal travel cost: %.3f", optimalMeanCost)
}
