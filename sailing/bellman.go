package main

import (
	"math"
)

func bellman(size int, eps float64) float64 {
	// x, y, t, w
	cost := make([][][][]float64, size)
	for x := 0; x != size; x++ {
		cost[x] = make([][][]float64, size)
		for y := 0; y != size; y++ {
			cost[x][y] = make([][]float64, 3) // tack
			c0 := math.MaxFloat64
			if x == size-1 && y == size-1 {
				c0 = 0.
			}
			for t := -1; t != 2; t++ {
				cost[x][y][t+1] = make([]float64, 8) // winds
				for w := 0; w != nDir; w++ {
					cost[x][y][t+1][w] = c0
				}
			}
		}
	}

	converged := false
	for !converged {
		converged = true
		for x := size - 1; x != -1; x-- {
			for y := size - 1; y != -1; y-- {
				if x == size-1 && y == size-1 {
					continue
				}
				for t := -1; t != 2; t++ {
					for w := 0; w != nDir; w++ {
						cmin := math.MaxFloat64
						for l := 0; l != nDir; l++ {
							xnext := x + int(dirs[l][0])
							ynext := y + int(dirs[l][1])
							if xnext == -1 || xnext == size ||
								ynext == -1 || ynext == size {
								continue
							}
							c := legCost(t, l, w)
							tnext := tacks[l][w]
							for wnext := 0; wnext != nDir; wnext++ {
								c += pWind[w][wnext] *
									cost[xnext][ynext][tnext+1][wnext]
							}
							if c < cmin {
								cmin = c
							}
						}
						if math.Abs(cost[x][y][t+1][w]-cmin) > eps {
							converged = false
						}
						cost[x][y][t+1][w] = cmin
					}
				}
			}
		}
	}

	// no tack, any wind
	meanCost, n := 0., 0.
	for w := 0; w != nDir; w++ {
		meanCost += cost[0][0][1][w]
		n++
	}
	meanCost /= n

	return meanCost
}
