package main

import (
	"testing"
)

func TestTacks(tst *testing.T) {
	for i, c := range []struct {
		l, w, t int
	}{
		{0, 0, 0},
		{0, 1, 1},
		{1, 0, -1},
		{1, 0, -1},
		{0, 4, 0},
		{4, 0, 0},
	} {
		t := tacks[c.l][c.w]
		if t != c.t {
			tst.Errorf("%d: got %d, want %d", i, t, c.t)
		}
	}
}

func TestCosts(t *testing.T) {
	for i, cse := range []struct {
		l, w int
		c    float64
	}{
		{0, 0, awayCost},
		{0, 4, intoCost},
		{1, 0, downCost},
		{0, 1, downCost},
		{0, 2, crossCost},
		{0, 3, upCost},
	} {
		c := costs[cse.l][cse.w]
		if c != cse.c {
			t.Errorf("%d: got %g, want %g", i, c, cse.c)
		}
	}
}

func TestLegCost(t *testing.T) {
	for i, cse := range []struct {
		t, l, w int
		c       float64
	}{
		{0, 0, 0, awayCost},
		{1, 0, 3, upCost},
		{1, 0, 2, crossCost},
		{-1, 0, 2, delayCost + crossCost},
	} {
		c := legCost(cse.t, cse.l, cse.w)
		if c != cse.c {
			t.Errorf("%d: got %g, want %g", i, c, cse.c)
		}
	}
}

func TestMyopic(t *testing.T) {
	for i, c := range []struct {
		avgCost float64
		size    int
		p       pos
		t, w    int
		l       int
	}{
		{1, 2, pos{0, 0}, 1, 0, 0},
		{2, 2, pos{0, 0}, -1, 0, 1},
		{1, 2, pos{0, 0}, -1, 5, 0},
		{1, 2, pos{0, 0}, 1, 5, 2},
	} {
		l := (&myopic{c.avgCost}).leg(c.size, c.p, c.t, c.w)
		if l != c.l {
			t.Errorf("%d: got %d, want %d", i, l, c.l)
		}
	}
}
