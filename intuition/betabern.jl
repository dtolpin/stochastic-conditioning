### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 78a218bc-8bfb-11eb-078c-773e8ffd142a
using Turing, Distributions, Statistics, Plots, Random, KernelDensity


# ╔═╡ 04a46386-8bfe-11eb-08de-655923faffda
import Pkg; Pkg.add("KernelDensity")

# ╔═╡ 8df6ab42-8bfb-11eb-3356-5f5df378553a
md"### Example problem — coin flip
"

# ╔═╡ 6d953a0a-8c01-11eb-3705-710e30d72ac3
begin
	nobs = [1, 5, 25]
	nsamples = 10000
	pobs = 0.75
	size=(600,200)
end

# ╔═╡ 9fb206ec-8bfb-11eb-2ce6-9799256b37f3
@model function three(y::Array{Float64,1})
	p ~ Beta(1, 1)
	for i = eachindex(y)
		Turing.acclogp!(_varinfo, y[i]*log(p) + (1 - y[i])*log(1-p))
	end
end

# ╔═╡ a7baf126-8bfb-11eb-3c5f-4d7d07b16bae
let plt = plot(size=size)
	for i in nobs
		chain = sample(three(pobs.*ones(i)), MH(), nsamples)
		plt = histogram!(plt, chain[:p], normalize=true, label=string(i), opacity=0.33, linewidth=0.5, bins=100, legend=:topleft)
	end
	savefig(plt, "three.pdf")
	plt
end

# ╔═╡ 9bf22660-8bfe-11eb-2be2-f1bd49231747
@model function four(y::Array{Float64,1})
	p ~ Beta(1, 1)
	for i = eachindex(y)
		Turing.acclogp!(_varinfo, log(y[i]*p + (1 - y[i])*(1-p)))
	end
end

# ╔═╡ 87e3b038-8c00-11eb-1af2-1917e2ad4af1
let plt = plot(size=size)
	for i in nobs
		chain = sample(four(pobs.*ones(i)), MH(), 
			nsamples)
		plt = histogram!(plt, chain[:p], normalize=true, label=string(i), opacity=0.33, linewidth=0.5, bins=100, legend=false)
	end
	savefig(plt, "four.pdf")
	plt
end

# ╔═╡ Cell order:
# ╠═78a218bc-8bfb-11eb-078c-773e8ffd142a
# ╠═04a46386-8bfe-11eb-08de-655923faffda
# ╠═8df6ab42-8bfb-11eb-3356-5f5df378553a
# ╠═6d953a0a-8c01-11eb-3705-710e30d72ac3
# ╠═9fb206ec-8bfb-11eb-2ce6-9799256b37f3
# ╠═a7baf126-8bfb-11eb-3c5f-4d7d07b16bae
# ╠═9bf22660-8bfe-11eb-2be2-f1bd49231747
# ╠═87e3b038-8c00-11eb-1af2-1917e2ad4af1
