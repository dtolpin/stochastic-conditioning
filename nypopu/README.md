# Estimating population of New York state

Run:

* `make` to build, 
* `make run` to run the experiment,
* `jupyter notebook posterior.ipynb` to open the posterior analysis notebook.
