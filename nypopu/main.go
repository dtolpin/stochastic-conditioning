package main

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	"encoding/csv"
	"flag"
	"fmt"
	"gonum.org/v1/gonum/stat/distuv"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"sort"
	. "stochastic-conditioning/infer"
	ad "stochastic-conditioning/nypopu/model/ad"
	. "stochastic-conditioning/nypopu/model"
	"strconv"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`infer population distribution in NY:
    %s [FLAGS] < SAMPLE.csv
`,
			os.Args[0])
		flag.PrintDefaults()
	}
	flag.Float64Var(&UNIT, "unit", UNIT, "population unit")
	flag.IntVar(&NBURN, "nburn", NBURN, "number of burned iterations")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&MITER, "miter", MITER, "number of inner iterations")
	flag.IntVar(&THIN, "thin", THIN, "output 1 of each <thin> samples")
	flag.IntVar(&NSTEPS, "nsteps", NSTEPS, "number of leapfrog steps")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	flag.BoolVar(&NESTED, "nested",  NESTED, "nested model")
	flag.Float64Var(&SIGMA, "sigma", SIGMA, "proposal standard deviation")

	log.SetFlags(0)
}

// Parameter defaults
var (
	UNIT   = 10000.
	NBURN  = 0
	NITER  = 10000
	MITER  = 0
	THIN   = 1
	NSTEPS = 5
	ETA    = 0.001
	ALPHA  = 0.1
	V      = 1.
	NESTED = false
	SIGMA  = 0.5
)

func main() {
	flag.Parse()
	if NBURN == 0 {
		NBURN = NITER
	}
	if MITER == 0 {
		for  MITER*MITER < NITER {
			MITER++
		}
	}

	if flag.NArg() != 0 {
		log.Fatalf("unexpected positional arguments: %v",
			flag.Args())
	}

	var (
		sm *ad.Model
		nm *NestedModel
	)
	if NESTED {
		nm = &NestedModel{MITER: MITER}
		sm = (*ad.Model)(&nm.Model)
	} else {
		sm = &ad.Model{}
	}
	var q [][2]float64

	// Read the data
	rdr := csv.NewReader(os.Stdin)
	for {
		rec, err := rdr.Read()
		if err == io.EOF {
			break
		}
		val, err := strconv.ParseFloat(rec[1], 64)
		if err != nil {
			log.Fatalf("cannot parse  %s %s: %v",
				rec[0], rec[1], err)
		}
		var prob float64
		switch rec[0] {
		case "count":
			sm.N = val
			continue
		case "total":
			// should be mean*count
			continue
		case "mean":
			sm.Mean = val / UNIT
			continue
		case "sd":
			sm.Vari = val / UNIT * val / UNIT
			continue
		case "lowest":
			prob = 0.
		case "median":
			prob = 0.5
		case "highest":
			prob = 1.
		default: // quantiles
			prob, err = strconv.ParseFloat(
				strings.TrimSuffix(rec[0], "%"), 64)
			if err != nil {
				log.Fatalf("cannot parse quantile %v: %v",
					rec[0], err)
			}
			prob /= 100.
		}
		// add quantil
		q = append(q, [2]float64{prob, val / UNIT})
	}
	sort.Slice(q, func(i, j int) bool {
		return q[i][0] < q[j][0]
	})
	switch {
	case q[0][0] != 0.:
		log.Fatalf("missing p=0: %v", q)
	case q[len(q)-1][0] != 1.:
		log.Fatalf("missing p=1: %v", q)
	}

	// Simulate according to quantiles
	sm.Z = make(chan float64, 1)
	go func() {
		for {
			sm.Z <- RandQuantile(q)
		}
	}()

	log.Printf("Model: %+v", sm)

	// Initialize the parameter vector
	x := make([]float64, 2)
	for i := range x {
		x[i] = 0.1 * rand.NormFloat64()
	}

	// Run observe once to catch coding errors
	var m model.Model
	var mcmc infer.MCMC
	if NESTED {
		nm.Observe(x)

		// Infer the posterior
		mcmc = &MH{
			Q: func(x, xn []float64) {
				for i := range x {
					xn[i] = x[i] + SIGMA*rand.NormFloat64()
				}
			},
		}
		m = nm
	} else {
		sm.Observe(x)
		model.DropGradient(sm)

		// Infer the posterior
		mcmc = &infer.SgHMC{
			L:     NSTEPS,
			Eta:   ETA,
			Alpha: ALPHA,
			V:     V,
		}
		m = sm
	}
	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)
	// Burn
	for i := 0; i != NBURN; i++ {
		<-samples
	}
	// Collect after burn-in
	wtr := csv.NewWriter(os.Stdout)
	for i := 0; i != NITER; i++ {
		var x []float64
		x = <-samples
		if len(x) == 0 {
			break
		}
		if (i+1)%THIN == 0 {
			var rec []string
			for i := range x {
				rec = append(rec, strconv.FormatFloat(x[i], 'f', -1, 64))
			}
			// add a single sample from the prior
			rec = append(rec,
				strconv.FormatFloat(RandQuantile(q), 'f', -1, 64))
			// add a single sample from the posterior
			rec = append(rec,
				strconv.FormatFloat(Sample(x), 'f', -1, 64))
			wtr.Write(rec)
			wtr.Flush()
		}
	}
	mcmc.Stop()
	if NESTED {
		mh, _ := mcmc.(*MH)
		log.Printf("MH acceptance rate: %.3f",
			float64(mh.NAcc)/float64(mh.NAcc+mh.NRej))
	}
}

// RandQuantile draws a random number from quantiles
func RandQuantile(q [][2]float64) float64 {
	p := rand.Float64()
	for i := 1; i != len(q); i++ {
		if q[i][0] >= p {
			return q[i-1][1] + rand.Float64()*(q[i][1]-q[i-1][1])
		}
	}
	panic("non-exhaustive quantiles")
}

func Sample(x []float64) float64 {
	mean := math.Exp(x[0])
	vari := math.Exp(x[1])

	sigma := math.Sqrt(math.Log(vari/(mean*mean) + 1))
	mu := math.Log(mean) - 0.5*sigma*sigma
	return distuv.LogNormal{Mu: mu, Sigma: sigma}.Rand()
}
