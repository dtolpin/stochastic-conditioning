package model

import (
	. "bitbucket.org/dtolpin/infergo/dist/ad"
	"bitbucket.org/dtolpin/infergo/ad"
	"fmt"
	"math"
)

type Model struct {
	N	float64
	Mean	float64
	Vari	float64
	Z	chan float64
}

func (m *Model) String() string {
	return fmt.Sprintf("{N:%.0f Mean:%.4g Vari:%.4g}",
		m.N, m.Mean, m.Vari)
}

func (m *Model) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var logp float64
	ad.Assignment(&logp, ad.Value(0.))
	var mean float64
	ad.Assignment(&mean, ad.Elemental(math.Exp, &x[0]))
	var vari float64
	ad.Assignment(&vari, ad.Elemental(math.Exp, &x[1]))
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpAdd, &x[0], ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, &m.Mean, ad.Elemental(math.Sqrt, ad.Arithmetic(ad.OpDiv, &m.Vari, &m.N)), &mean))))
	var sigma float64
	ad.Assignment(&sigma, ad.Elemental(math.Sqrt, ad.Elemental(math.Log, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpDiv, &vari, (ad.Arithmetic(ad.OpMul, &mean, &mean))), ad.Value(1)))))
	var mu float64
	ad.Assignment(&mu, ad.Arithmetic(ad.OpSub, ad.Elemental(math.Log, &mean), ad.Arithmetic(ad.OpMul, ad.Arithmetic(ad.OpMul, ad.Value(0.5), &sigma), &sigma)))
	for i := 0.; i != m.N; i = i + 1 {
		var z float64
		ad.Assignment(&z, ad.Value(<-m.Z))
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpNeg, ad.Elemental(math.Log, &z)), ad.Call(func(_ []float64) {
			Normal.Logp(0, 0, 0)
		}, 3, &mu, &sigma, ad.Elemental(math.Log, &z)))))
	}

	return ad.Return(&logp)
}

type NestedModel struct {
	Model
	MITER	int
}

func (m *NestedModel) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var logp float64
	ad.Assignment(&logp, ad.Value(0.))
	var mean float64
	ad.Assignment(&mean, ad.Elemental(math.Exp, &x[0]))
	var vari float64
	ad.Assignment(&vari, ad.Elemental(math.Exp, &x[1]))
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpAdd, &x[0], ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, &m.Mean, ad.Elemental(math.Sqrt, ad.Arithmetic(ad.OpDiv, &m.Vari, &m.N)), &mean))))
	var ll float64
	ad.Assignment(&ll, ad.Value(0.))
	var sigma float64
	ad.Assignment(&sigma, ad.Elemental(math.Sqrt, ad.Elemental(math.Log, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpDiv, &vari, (ad.Arithmetic(ad.OpMul, &mean, &mean))), ad.Value(1)))))
	var mu float64
	ad.Assignment(&mu, ad.Arithmetic(ad.OpSub, ad.Elemental(math.Log, &mean), ad.Arithmetic(ad.OpMul, ad.Arithmetic(ad.OpMul, ad.Value(0.5), &sigma), &sigma)))
	for j := 0; j != m.MITER; j = j + 1 {
		for i := 0.; i != m.N; i = i + 1 {
			var z float64
			ad.Assignment(&z, ad.Value(<-m.Z))
			ad.Assignment(&ll, ad.Arithmetic(ad.OpAdd, &ll, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpNeg, ad.Elemental(math.Log, &z)), ad.Call(func(_ []float64) {
				Normal.Logp(0, 0, 0)
			}, 3, &mu, &sigma, ad.Elemental(math.Log, &z)))))
		}
	}
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpDiv, &ll, ad.Value(float64(m.MITER)))))

	return ad.Return(&logp)
}
