package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"fmt"
	"math"
)

type Model struct {
	N    float64      // number of observations
	Mean float64      // mean population
	Vari float64      // population variance
	Z    chan float64 // simulated populations
}

func (m *Model) String() string {
	return fmt.Sprintf("{N:%.0f Mean:%.4g Vari:%.4g}",
		m.N, m.Mean, m.Vari)
}

func (m *Model) Observe(x []float64) float64 {
	logp := 0.
	mean := math.Exp(x[0])
	vari := math.Exp(x[1])
	logp += x[0] + Normal.Logp(m.Mean, math.Sqrt(m.Vari/m.N), mean)

	// Stochastic conditioning, log-normal
	sigma := math.Sqrt(math.Log(vari/(mean*mean) + 1))
	mu := math.Log(mean) - 0.5*sigma*sigma
	for i := 0.; i != m.N; i++ {
		z := <-m.Z
		logp += -math.Log(z) + Normal.Logp(mu, sigma, math.Log(z))
	}

	return logp
}

type NestedModel struct {
	Model
	MITER int
}

func (m *NestedModel) Observe(x []float64) float64 {
	logp := 0.
	mean := math.Exp(x[0])
	vari := math.Exp(x[1])
	logp += x[0] + Normal.Logp(m.Mean, math.Sqrt(m.Vari/m.N), mean)

	// Stochastic conditioning, log-normal
	ll := 0.
	sigma := math.Sqrt(math.Log(vari/(mean*mean) + 1))
	mu := math.Log(mean) - 0.5*sigma*sigma
	for j := 0; j != m.MITER; j++ {
		for i := 0.; i != m.N; i++ {
			z := <-m.Z
			ll += -math.Log(z) + Normal.Logp(mu, sigma, math.Log(z))
		}
	}
	logp += ll/float64(m.MITER)

	return logp
}
