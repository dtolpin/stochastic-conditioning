package infer

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	"log"
	"math"
	"math/rand"
)

// Metropolis-Hastings
type MH struct {
	infer.Sampler

	// Parameters
	Q    func(x, xnext []float64) // parameter proposal
}

func (mh *MH) Sample(
	m model.Model,
	x []float64,
	samples chan []float64,
) {
	mh.Samples = samples
	go func() {
		// On exit:
		// * close samples;
		defer close(samples)
		// * intercept errors deep inside the algorithm
		// and report them.
		defer func() {
			if r := recover(); r != nil {
				log.Printf("ERROR: MH: %v", r)
			}
		}()
		l := m.Observe(x)
		xn := make([]float64, len(x))
		for {
			if mh.Stopped {
				break
			}

			// Propose
			mh.Q(x, xn)

			// Evaluate and accept/reject
			ln := m.Observe(xn)
			u := math.Log(1. - rand.Float64())
			log.Printf("x=%.3f xn=%.3f l=%.3f ln=%.3f %v", x, xn, l, ln, ln-l > u)
			if ln-l > u {
				copy(x, xn)
				l = ln
				mh.NAcc++
			} else {
				mh.NRej++
			}

			// Write the sample
			samples <- x
		}
	}()
}
