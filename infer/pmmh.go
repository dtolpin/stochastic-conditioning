package infer

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"log"
	"math"
	"math/rand"
	. "stochastic-conditioning/model"
)

// Pseudo-marginal Metropolis-Hastings
type PMMH struct {
	infer.Sampler

	// Parameters
	Q    func(x, xnext []float64) // parameter proposal
	NPar int                      // number of particles
}

func (mh *PMMH) Sample(
	m ParticleModel,
	x []float64,
	samples chan []float64,
) {
	mh.Samples = samples
	go func() {
		// On exit:
		// * close samples;
		defer close(samples)
		// * intercept errors deep inside the algorithm
		// and report them.
		defer func() {
			if r := recover(); r != nil {
				log.Printf("ERROR: MH: %v", r)
			}
		}()
		xn := make([]float64, len(x))
		for {
			if mh.Stopped {
				break
			}

			// Create particles
			p := make([]interface{}, mh.NPar)
			for i := range p {
				p[i] = m.NewParticle()
			}

			// Propose
			mh.Q(x, xn)

			// Evaluate and accept/reject
			l := mh.observe(m, x, p)
			ln := mh.observe(m, xn, p)
			u := math.Log(1. - rand.Float64())
			if ln-l > u {
				copy(x, xn)
				mh.NAcc++
			} else {
				mh.NRej++
			}

			// Write the sample
			samples <- x
		}
	}()
}

func (mh *PMMH) observe(
	m ParticleModel,
	x []float64,
	p []interface{},
) (logp float64) {
	// compute logp estimate for x.
	s, s2, n := 0., 0., 0.
	for i := range p {
		l := m.Observe(x, p[i])
		s += l
		s2 += l * l
		n += 1.
	}
	s /= n
	s2 /= n
	logp = s - (s2-s*s)/(n-1.)
	return logp
}
