package main

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/mathx"
	"bitbucket.org/dtolpin/infergo/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	. "stochastic-conditioning/commute/model"
	ad "stochastic-conditioning/commute/model/ad"
	. "stochastic-conditioning/commute/problem"
	. "stochastic-conditioning/infer"
	"strconv"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`infer forecast accuracy:
    %s [flags]
`,
			os.Args[0])
		flag.PrintDefaults()
	}
	flag.StringVar(&COND, "cond", COND, "conditioning, one of D, A, S, N, I")
	flag.Float64Var(&PR, "pr", PR, "probability of rain")
	flag.Float64Var(&PT, "pt", PT, "probability of true positive")
	flag.Float64Var(&PF, "pf", PF, "probability of false positive")
	flag.IntVar(&NBURN, "nburn", NBURN, "number of burned iterations")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&MITER, "miter", MITER, "number of inner iterations")
	flag.IntVar(&THIN, "thin", THIN, "output 1 of each <thin> samples")
	flag.IntVar(&NSTEPS, "nsteps", NSTEPS, "number of leapfrog steps")
	flag.Float64Var(&EPS, "eps", EPS, "leapfrog step size")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	flag.Float64Var(&SIGMA, "sigma", SIGMA, "proposal standard deviation")

	log.SetFlags(0)
}

var (
	COND   = "D"
	PR     = Prain
	PT     = Ptrue
	PF     = Pfalse
	NBURN  = 0
	NITER  = 10000
	MITER  = 0
	THIN   = 0
	NSTEPS = 5
	EPS    = 0.5

	// sgHMC
	ETA   = 0.001
	ALPHA = 0.1
	V     = 1.

	SIGMA  = 0.5
)

func main() {
	flag.Parse()
	if NBURN == 0 {
		NBURN = NITER
	}
	if MITER == 0 {
		for  MITER*MITER < NITER {
			MITER++
		}
	}

	if flag.NArg() != 0 {
		log.Fatalf("unexpected positional arguments: %v",
			flag.Args())
	}
	COND = strings.ToUpper(COND)
	switch COND {
	case "D", "A", "S", "N", "I":
	default:
		log.Fatalf("Invalid conditioning, want D, A, S, or I, got %q:",
			COND)
	}

	// Read the data
	var data []Entry
	rdr := csv.NewReader(os.Stdin)
	rdr.Read()
	for {
		rec, err := rdr.Read()
		if err == io.EOF {
			break
		}
		rain, err := strconv.ParseBool(rec[0])
		if err != nil {
			log.Fatalf("invalid rain %q: %v", rec[0], err)
		}
		duration, err := strconv.ParseFloat(rec[1], 64)
		if err != nil {
			log.Fatalf("invalid duration %q: %v", rec[1], err)
		}
		data = append(data, Entry{rain, duration})
	}

	var m model.Model
	switch COND {
	case "D":
		m = &ModelD{Data: data}
	case "A":
		prain := 0.
		durations := make([]float64, len(data))
		for i := range data {
			if data[i].Rain {
				prain += 1. / float64(len(data))
			}
			durations[i] = data[i].Duration
		}
		m = &ad.ModelA{
			Prain:     prain,
			Durations: durations,
		}
	case "S":
		rains := make(chan bool, 1)
		durations := make(chan float64, 1)
		m = &ad.ModelS{
			NObs:         float64(len(data)),
			DistRain:     rains,
			DistDuration: durations,
		}

		// Decouple rain and duration
		go func() {
			for {
				rains <- data[rand.Intn(len(data))].Rain
				durations <- data[rand.Intn(len(data))].Duration
			}
		}()

	case "N":
		rains := make(chan bool, 1)
		durations := make(chan float64, 1)
		m = &ModelN{
			ModelS: ModelS {
				NObs:         float64(len(data)),
				DistRain:     rains,
				DistDuration: durations,
			},
			MITER: MITER,
		}

		// Decouple rain and duration
		go func() {
			for {
				durations <- data[rand.Intn(len(data))].Duration
			}
		}()
		go func() {
			for {
				rains <- data[rand.Intn(len(data))].Rain
			}
		}()


	case "I":
		intensities := make(chan float64, 1)
		durations := make(chan float64, 1)
		m = &ad.ModelI{
			NObs:          float64(len(data)),
			DistIntensity: intensities,
			DistDuration:  durations,
		}

		// Decouple rain and duration
		go func() {
			for {
				rain := data[rand.Intn(len(data))].Rain
				if rain {
					// Precipitation on wet days is often
					// modelled by exponential distribution
					intensities <- rand.ExpFloat64()
				} else {
					intensities <- 0
				}
				durations <- data[rand.Intn(len(data))].Duration
			}
		}()
	}

	// Initialize the parameter vector
	x := make([]float64, 3)
	for i := range x {
		x[i] = 0.1 * rand.NormFloat64()
	}

	m.Observe(x)
	switch COND {
	case "N":
	default:
		model.DropGradient(m)
	}

	// Infer the posterior
	fmt.Println("p_t,p_f")

	var mcmc infer.MCMC
	switch COND {
	case "D", "A":
		mcmc = &infer.HMC{
			L:   NSTEPS,
			Eps: EPS,
		}
	case "S", "I":
		mcmc = &infer.SgHMC{
			L:     NSTEPS,
			Eta:   ETA,
			Alpha: ALPHA,
			V:     V,
		}
	case "N":
		mcmc = &MH {
			Q: func(x, xn []float64) {
				for i := range x {
					xn[i] = x[i] + SIGMA*rand.NormFloat64()
				}
			},
		}
	}
	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)
	// Burn
	nburn := NBURN
	niter := NITER
	if THIN == 0 {
		switch COND {
		case "S", "I":
			// Same amount of computation
			THIN = len(data)
			niter *= THIN
			nburn *= THIN
		}
	}
	for i := 0; i != nburn; i++ {
		<-samples
	}

	// Collect after burn-in
	s_t, s2_t, s_f, s2_f := 0., 0., 0., 0.
	n := 0.
	for i := 0; i != niter; i++ {
		var x []float64
		x = <-samples
		if len(x) == 0 {
			break
		}
		p_t := mathx.Sigm(x[1])
		p_f := mathx.Sigm(x[2])
		if (i+1)%THIN != 0 {
			continue
		}
		fmt.Printf("%f,%f\n", p_t, p_f)
		s_t += p_t
		s2_t += p_t * p_t
		s_f += p_f
		s2_f += p_f * p_f
		n++
	}
	mcmc.Stop()

	// Output mean estimates of expected times
	p_t_mean := s_t / n
	p_t_vari := s2_t/n - p_t_mean*p_t_mean
	p_f_mean := s_f / n
	p_f_vari := s2_f/n - p_f_mean*p_f_mean
	log.Printf("cond: %s, p_f: %.3f(%.3f±%.3f), p_t: %.3f(%.3f±%.3f)",
		COND,
		mode(p_f_mean, p_f_vari), p_f_mean, math.Sqrt(p_f_vari),
		mode(p_t_mean, p_t_vari), p_t_mean, math.Sqrt(p_t_vari))

	switch COND {
	case "N":
		mh, _ := mcmc.(*MH)
		log.Printf("MH acceptance rate: %.3f",
			float64(mh.NAcc)/float64(mh.NAcc+mh.NRej))
	}
}

func mode(mean, vari float64) float64 {
	alpha := mean*mean*(1-mean)/vari - mean
	beta := alpha/mean - alpha
	return math.Max(0, math.Min(1,
		(alpha-1)/(alpha+beta-2)))
}
