package simu

import (
	"math/rand"
	. "stochastic-conditioning/commute/problem"
)

func Sample(pr, pt, pf float64) (
	rain bool,
	duration float64,
) {
	rain = rand.Float64() <= pr
	var willRain bool
	if rain {
		willRain = rand.Float64() <= pt
	} else {
		willRain = rand.Float64() <= pf
	}
	var mu, sigma float64
	switch {
	case willRain:
		mu, sigma = MuTaxi, SigmaTaxi
	case !rain:
		mu, sigma = MuMoto, SigmaMoto
	default:
		mu, sigma = MuMotoRain, SigmaMotoRain
	}
	duration = rand.NormFloat64()*sigma + mu

	return rain, duration
}
