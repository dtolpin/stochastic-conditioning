package problem

const (
	MuMoto, SigmaMoto         = 15, 2
	MuMotoRain, SigmaMotoRain = 60, 8
	MuTaxi, SigmaTaxi         = 30, 4

	// For simulation
	Prain  = 0.2
	Ptrue  = 0.8
	Pfalse = 0.1
)
