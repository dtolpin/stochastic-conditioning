package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	. "stochastic-conditioning/commute/problem"
	"stochastic-conditioning/commute/simu"
	"time"
)

var (
	N  = 100
	PR = Prain
	PT = Ptrue
	PF = Pfalse
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`Simulate observations:
    %s [flags]
`,
			os.Args[0])
		flag.PrintDefaults()
	}
	flag.IntVar(&N, "n", N, "number of observations")
	flag.Float64Var(&PR, "pr", PR, "probability of rain")
	flag.Float64Var(&PT, "pt", PT, "probability of true positive")
	flag.Float64Var(&PF, "pf", PF, "probability of false positive")
}

func main() {
	flag.Parse()
	fmt.Println("rain,duration")
	for i := 0; i != N; i++ {
		rain, duration := simu.Sample(PR, PT, PF)
		fmt.Printf("%v,%.3f\n", rain, duration)
	}
}
