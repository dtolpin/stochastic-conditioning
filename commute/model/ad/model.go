package model

import (
	. "bitbucket.org/dtolpin/infergo/dist/ad"
	"bitbucket.org/dtolpin/infergo/ad"
	"bitbucket.org/dtolpin/infergo/mathx"
	"math"
	. "stochastic-conditioning/commute/problem"
)

const (
	alpha	= 2
	beta	= alpha
)

type Entry struct {
	Rain		bool
	Duration	float64
}

type ModelD struct {
	Data []Entry
}

func (m ModelD) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var p_r float64
	ad.Assignment(&p_r, ad.Elemental(mathx.Sigm, &x[0]))
	var p_t float64
	ad.Assignment(&p_t, ad.Elemental(mathx.Sigm, &x[1]))
	var p_f float64
	ad.Assignment(&p_f, ad.Elemental(mathx.Sigm, &x[2]))
	var logp float64
	ad.Assignment(&logp, ad.Call(func(_vararg []float64) {
		Beta.Logps(0, 0, _vararg...)
	}, 2, ad.Value(alpha), ad.Value(beta), &p_r, &p_t, &p_f))

	for i := range m.Data {
		var rain bool

		rain = m.Data[i].Rain
		var duration float64
		ad.Assignment(&duration, &m.Data[i].Duration)
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Call(func(_ []float64) {
			Sim.logpb(0, 0, 0, rain, 0)
		}, 4, &p_r, &p_t, &p_f, &duration)))
	}

	return ad.Return(&logp)
}

type ModelA struct {
	Prain		float64
	Durations	[]float64
}

func (m ModelA) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var p_r float64
	ad.Assignment(&p_r, ad.Elemental(mathx.Sigm, &x[0]))
	var p_t float64
	ad.Assignment(&p_t, ad.Elemental(mathx.Sigm, &x[1]))
	var p_f float64
	ad.Assignment(&p_f, ad.Elemental(mathx.Sigm, &x[2]))
	var logp float64
	ad.Assignment(&logp, ad.Call(func(_vararg []float64) {
		Beta.Logps(0, 0, _vararg...)
	}, 2, ad.Value(alpha), ad.Value(beta), &p_r, &p_t, &p_f))

	for _, duration := range m.Durations {
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpMul, &m.Prain, ad.Call(func(_ []float64) {
			Sim.logpb(0, 0, 0, true, 0)
		}, 4, &p_r, &p_t, &p_f, &duration))))
		ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpMul, (ad.Arithmetic(ad.OpSub, ad.Value(1), &m.Prain)), ad.Call(func(_ []float64) {
			Sim.logpb(0, 0, 0, false, 0)
		}, 4, &p_r, &p_t, &p_f, &duration))))
	}

	return ad.Return(&logp)
}

type ModelS struct {
	NObs		float64
	DistRain	<-chan bool
	DistDuration	<-chan float64
}

func (m ModelS) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var rain bool

	rain = <-m.DistRain
	var duration float64
	ad.Assignment(&duration, ad.Value(<-m.DistDuration))
	var p_r float64
	ad.Assignment(&p_r, ad.Elemental(mathx.Sigm, &x[0]))
	var p_t float64
	ad.Assignment(&p_t, ad.Elemental(mathx.Sigm, &x[1]))
	var p_f float64
	ad.Assignment(&p_f, ad.Elemental(mathx.Sigm, &x[2]))
	var logp float64
	ad.Assignment(&logp, ad.Call(func(_vararg []float64) {
		Beta.Logps(0, 0, _vararg...)
	}, 2, ad.Value(alpha), ad.Value(beta), &p_r, &p_t, &p_f))

	if m.NObs == 0 {
		ad.Assignment(&m.NObs, ad.Value(1))
	}
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpMul, &m.NObs, ad.Call(func(_ []float64) {
		Sim.logpb(0, 0, 0, rain, 0)
	}, 4, &p_r, &p_t, &p_f, &duration))))

	return ad.Return(&logp)
}

type ModelN struct {
	ModelS
	MITER	int
}

func (m ModelN) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var p_r float64
	ad.Assignment(&p_r, ad.Elemental(mathx.Sigm, &x[0]))
	var p_t float64
	ad.Assignment(&p_t, ad.Elemental(mathx.Sigm, &x[1]))
	var p_f float64
	ad.Assignment(&p_f, ad.Elemental(mathx.Sigm, &x[2]))
	var logp float64
	ad.Assignment(&logp, ad.Call(func(_vararg []float64) {
		Beta.Logps(0, 0, _vararg...)
	}, 2, ad.Value(alpha), ad.Value(beta), &p_r, &p_t, &p_f))
	var ll float64
	ad.Assignment(&ll, ad.Value(0.))
	for i := 0; i != m.MITER; i = i + 1 {
		var rain bool

		rain = <-m.DistRain
		for j := 0; j != m.MITER; j = j + 1 {
			var duration float64
			ad.Assignment(&duration, ad.Value(<-m.DistDuration))

			if m.NObs == 0 {
				ad.Assignment(&m.NObs, ad.Value(1))
			}
			ad.Assignment(&ll, ad.Arithmetic(ad.OpAdd, &ll, ad.Arithmetic(ad.OpMul, &m.NObs, ad.Call(func(_ []float64) {
				Sim.logpb(0, 0, 0, rain, 0)
			}, 4, &p_r, &p_t, &p_f, &duration))))
		}
	}
	ad.Assignment(&ll, ad.Arithmetic(ad.OpDiv, &ll, ad.Value(float64(m.MITER*m.MITER))))
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, &ll))

	return ad.Return(&logp)
}

type ModelI struct {
	NObs		float64
	DistIntensity	<-chan float64
	DistDuration	<-chan float64
}

func (m ModelI) Observe(x []float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup(x)
	}
	var intensity float64
	ad.Assignment(&intensity, ad.Value(<-m.DistIntensity))
	var duration float64
	ad.Assignment(&duration, ad.Value(<-m.DistDuration))
	var p_r float64
	ad.Assignment(&p_r, ad.Elemental(mathx.Sigm, &x[0]))
	var p_t float64
	ad.Assignment(&p_t, ad.Elemental(mathx.Sigm, &x[1]))
	var p_f float64
	ad.Assignment(&p_f, ad.Elemental(mathx.Sigm, &x[2]))
	var logp float64
	ad.Assignment(&logp, ad.Call(func(_vararg []float64) {
		Beta.Logps(0, 0, _vararg...)
	}, 2, ad.Value(alpha), ad.Value(beta), &p_r, &p_t, &p_f))

	if m.NObs == 0 {
		ad.Assignment(&m.NObs, ad.Value(1))
	}
	ad.Assignment(&logp, ad.Arithmetic(ad.OpAdd, &logp, ad.Arithmetic(ad.OpMul, &m.NObs, ad.Call(func(_ []float64) {
		Sim.logpi(0, 0, 0, 0, 0)
	}, 5, &p_r, &p_t, &p_f, &intensity, &duration))))

	return ad.Return(&logp)
}

type sim struct{}

func (sim) Observe([]float64) float64 {
	if ad.Called() {
		ad.Enter()
	} else {
		ad.Setup([]float64{})
	}
	return ad.Return(ad.Value(0.))
}

var Sim sim

func (sim) logpb(
	p_r, p_t, p_f float64,
	rain bool,
	duration float64,
) (logp float64) {
	if ad.Called() {
		ad.Enter(&p_r, &p_t, &p_f, &duration)
	} else {
		panic("logpb called outside Observe")
	}

	var intensity float64
	if rain {
		ad.Assignment(&intensity, ad.Value(1))
	} else {
		ad.Assignment(&intensity, ad.Value(0))
	}
	return ad.Return(ad.Call(func(_ []float64) {
		Sim.logpi(0, 0, 0, 0, 0)
	}, 5, &p_r, &p_t, &p_f, &intensity, &duration))
}

func (sim) logpi(
	p_r, p_t, p_f float64,
	intensity float64,
	duration float64,
) (logp float64) {
	if ad.Called() {
		ad.Enter(&p_r, &p_t, &p_f, &intensity, &duration)
	} else {
		panic("logpi called outside Observe")
	}

	var (
		logpIntensity	float64
		pWillRain	float64
	)
	if intensity > 0 {
		ad.Assignment(&logpIntensity, ad.Arithmetic(ad.OpSub, ad.Elemental(math.Log, &p_r), &intensity))
		ad.Assignment(&pWillRain, &p_t)
	} else {
		ad.Assignment(&logpIntensity, ad.Elemental(math.Log, ad.Arithmetic(ad.OpSub, ad.Value(1), &p_r)))
		ad.Assignment(&pWillRain, &p_f)
	}
	var logpDurationWillRain float64
	ad.Assignment(&logpDurationWillRain, ad.Arithmetic(ad.OpAdd, ad.Elemental(math.Log, &pWillRain), ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, ad.Value(MuTaxi), ad.Value(SigmaTaxi), &duration)))
	var logpDurationWillNotRain float64
	ad.Assignment(&logpDurationWillNotRain, ad.Arithmetic(ad.OpAdd, ad.Elemental(math.Log, ad.Arithmetic(ad.OpSub, ad.Value(1), &pWillRain)), ad.Call(func(_ []float64) {
		Normal.Logp(0, 0, 0)
	}, 3, ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpMul, (ad.Arithmetic(ad.OpSub, ad.Value(1), &intensity)), ad.Value(MuMoto)), ad.Arithmetic(ad.OpMul, &intensity, ad.Value(MuMotoRain))), ad.Arithmetic(ad.OpAdd, ad.Arithmetic(ad.OpMul, (ad.Arithmetic(ad.OpSub, ad.Value(1), &intensity)), ad.Value(SigmaMoto)), ad.Arithmetic(ad.OpMul, &intensity, ad.Value(SigmaMotoRain))), &duration)))
	var logpDuration float64
	ad.Assignment(&logpDuration, ad.Elemental(mathx.LogSumExp, &logpDurationWillRain, &logpDurationWillNotRain))

	return ad.Return(ad.Arithmetic(ad.OpAdd, &logpIntensity, &logpDuration))
}
