package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/mathx"
	"math"
	. "stochastic-conditioning/commute/problem"
)

const (
	alpha = 2 // Concentration of Beta prior
	beta  = alpha
)

// Deterministic conditioning: rain occurence and trip duration
// are known for each day.
type Entry struct {
	Rain     bool
	Duration float64
}

type ModelD struct {
	Data []Entry
}

func (m ModelD) Observe(x []float64) float64 {
	p_r := mathx.Sigm(x[0])
	p_t := mathx.Sigm(x[1])
	p_f := mathx.Sigm(x[2])

	logp := Beta.Logps(alpha, beta, p_r, p_t, p_f)

	for i := range m.Data {
		rain := m.Data[i].Rain
		duration := m.Data[i].Duration
		logp += Sim.logpb(p_r, p_t, p_f, rain, duration)
	}

	return logp
}

// Averaged conditioning: this works because the rain domain
// has only 2 values, false and true.
type ModelA struct {
	Prain     float64
	Durations []float64
}

func (m ModelA) Observe(x []float64) float64 {
	p_r := mathx.Sigm(x[0])
	p_t := mathx.Sigm(x[1])
	p_f := mathx.Sigm(x[2])

	logp := Beta.Logps(alpha, beta, p_r, p_t, p_f)

	for _, duration := range m.Durations {
		logp += m.Prain * Sim.logpb(p_r, p_t, p_f, true, duration)
		logp += (1 - m.Prain) * Sim.logpb(p_r, p_t, p_f, false, duration)
	}

	return logp
}

// Stochastic conditioning: marginal distributions of rain and
// trip durations are provided.
type ModelS struct {
	NObs         float64
	DistRain     <-chan bool
	DistDuration <-chan float64
}

func (m ModelS) Observe(x []float64) float64 {
	rain := <-m.DistRain
	duration := <-m.DistDuration

	p_r := mathx.Sigm(x[0])
	p_t := mathx.Sigm(x[1])
	p_f := mathx.Sigm(x[2])

	logp := Beta.Logps(alpha, beta, p_r, p_t, p_f)

	if m.NObs == 0 {
		m.NObs = 1
	}
	logp += m.NObs * Sim.logpb(p_r, p_t, p_f, rain, duration)

	return logp
}

// Nested model, based on ModelS
type ModelN struct {
	ModelS
	MITER int
}

func (m ModelN) Observe(x []float64) float64 {
	p_r := mathx.Sigm(x[0])
	p_t := mathx.Sigm(x[1])
	p_f := mathx.Sigm(x[2])

	logp := Beta.Logps(alpha, beta, p_r, p_t, p_f)

	ll := 0.
	for i := 0; i != m.MITER; i++ {
		rain := <-m.DistRain
		for j := 0; j != m.MITER; j++ {
			duration := <-m.DistDuration

			if m.NObs == 0 {
				m.NObs = 1
			}
			ll += m.NObs * Sim.logpb(p_r, p_t, p_f, rain, duration)
		}
	}
	ll /= float64(m.MITER*m.MITER)
	logp += ll

	return logp
}


// Stochastic conditioning with rain intensity:
// marginal distributions of rain intensity and
// trip durations are provided.
type ModelI struct {
	NObs          float64
	DistIntensity <-chan float64
	DistDuration  <-chan float64
}

func (m ModelI) Observe(x []float64) float64 {
	intensity := <-m.DistIntensity
	duration := <-m.DistDuration

	p_r := mathx.Sigm(x[0])
	p_t := mathx.Sigm(x[1])
	p_f := mathx.Sigm(x[2])

	logp := Beta.Logps(alpha, beta, p_r, p_t, p_f)

	if m.NObs == 0 {
		m.NObs = 1
	}
	logp += m.NObs * Sim.logpi(p_r, p_t, p_f, intensity, duration)

	return logp
}

// Singleton for implementing the simulator shared by both models
type sim struct{}

func (sim) Observe([]float64) float64 { return 0. }

var Sim sim

// Binary version of the simulator --- any rain causes the same  slowdown
func (sim) logpb(
	p_r, p_t, p_f float64,
	rain bool,
	duration float64,
) (logp float64) {

	var intensity float64
	if rain {
		intensity = 1
	} else {
		intensity = 0
	}
	return Sim.logpi(p_r, p_t, p_f, intensity, duration)
}

//  Continuous version of the simulator - varying intensity
func (sim) logpi(
	p_r, p_t, p_f float64,
	intensity float64,
	duration float64,
) (logp float64) {
	var (
		logpIntensity float64
		pWillRain     float64
	)
	if intensity > 0 {
		logpIntensity = math.Log(p_r) - intensity
		pWillRain = p_t
	} else {
		logpIntensity = math.Log(1 - p_r)
		pWillRain = p_f
	}

	logpDurationWillRain := math.Log(pWillRain) +
		Normal.Logp(MuTaxi, SigmaTaxi, duration)
	logpDurationWillNotRain := math.Log(1-pWillRain) +
		Normal.Logp(
			(1-intensity)*MuMoto+intensity*MuMotoRain,
			(1-intensity)*SigmaMoto+intensity*SigmaMotoRain,
			duration)

	logpDuration := mathx.LogSumExp(
		logpDurationWillRain,
		logpDurationWillNotRain)

	return logpIntensity + logpDuration
}
