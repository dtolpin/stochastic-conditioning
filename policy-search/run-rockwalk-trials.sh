#!/bin/sh

N=${N:-100000}
M=${M:-2500}
T=${T:-100 30 10 3 1 0.3 0.1 0.03 0.01 0.003 0.001}
I=${I:-4x4 5x5 7x8 10x10}

for t in $T; do
	for i in $I; do
		lein run -m rockwalk.core trials-smh --temperature $t --number-of-mh-iterations $N --number-of-test-episodes $M --instance-id $i
	done
done
