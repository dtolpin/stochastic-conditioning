#!/bin/sh

N=${N:-100000}
M=${M:-10000}
T=${T:-1 0.3 0.1 0.03 0.01 0.003 0.001}
P=${P:-0.6 0.7 0.8 0.9 1.0}

for t in $T; do
	for p in $P; do
		lein run -m ctp.core trials-smh --temperature $t --number-of-mh-iterations $N --number-of-test-episodes $M --open-probability $p -p edge-policy
	done
done
