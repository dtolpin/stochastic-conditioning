(ns rockwalk.core
  (:gen-class)
  (:require [clojure.tools.cli :as cli]
            [clojure.data.json :as json]
            [anglican
             [state :refer [get-predicts]]
             [core :refer [doquery]]
             [bbvb :refer [get-variational]]]
            [rockwalk
             [model :refer [rockwalk]]
             [data :refer [instances]]])
  (:use [anglican runtime emit]))


(def +instance-id+ :4x4)
(def +number-of-steps-range+ [1 2 5 10 20 50 100 200 500 1000])
(def +hed+ 10.0)
(def +number-of-mh-iterations+ 10000)
(def +number-of-test-episodes+ 1000)

(def +temperature+ 1.0)

(def +number-of-particles+ 1000)
(def +base-stepsize+ 0.1)
(def +adagrad+ 0.9)
(def +robbins-monro+ 0.5)

(defn export-instances []
  (spit "resources/rockwalk/instances.json"
      (json/write-str
       (into {}
             (map (fn [[id instance]]
                    [id (assoc instance
                          :rocks (into {}
                                       (map (fn [[loc _]]
                                              [loc loc])
                                            (:rocks instance))))])
                  instances)))))

(defn test-proposals
  "tests learned proposals by simulating
  a specified number of episodes"
  [proposals instance hed num-episodes]
  (let [;; generate i.i.d. samples of episodes
        ;; note: there are no gradient updates until
        ;; after the first num-episodes samples
        samples (->> #(first (doquery :bbeb rockwalk [instance hed]
                                      :only [:policy]
                                      :adagrad false
                                      :number-of-particles 1
                                      :initial-proposals proposals
                                      :stripdown false))
                     (repeatedly num-episodes))
        rewards (map (comp :reward get-predicts)
                     samples)
        visited (map (comp :visited get-predicts)
                     samples)
        rock-counts (frequencies (reduce concat visited))
        travel-counts (reduce
                       (fn [counts rocks]
                         (let [start [(:x instance) (:y instance)]
                               ;; append start node to sequenc of visited rocks
                               us (conj (seq rocks) start)
                               ;; append end node if last rock was not on edge
                               vs (if (= (first (last rocks)) (:n instance))
                                    rocks
                                    (conj (vec rocks)
                                          [(:n instance)
                                           (second (or (last rocks) start))]))]
                           (reduce
                            (fn [counts edge]
                              (update-in counts [edge] (fnil inc 0)))
                            counts
                            (map vector us vs))))
                       {}
                       visited)]
    [rewards rock-counts travel-counts]))

(defn trial
  [instance hed num-train-steps number-of-test-episodes
   & {number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}}]
  (let [state (->> (doquery :bbeb rockwalk [instance hed]
                            :only [:policy]
                            :number-of-particles number-of-particles
                            :base-stepsize base-stepsize
                            :adagrad adagrad
                            :robbins-monro robbins-monro
                            :stripdown false)
                   (drop (* num-train-steps number-of-particles))
                   first)
        proposals (get-variational state)]
    (conj (test-proposals proposals
                          instance
                          hed
                          number-of-test-episodes)
          (zipmap (map (comp vec rest) (keys proposals))
                  (map second (vals proposals))))))

(defn trial-lsmh
  [alg instance hed temperature
   number-of-mh-iterations number-of-test-episodes]
  (let [predicts (->> (doquery alg rockwalk [instance hed]
                               :temperature temperature)
                      (drop number-of-mh-iterations)
                      (take-nth (quot number-of-mh-iterations
                                      number-of-test-episodes))
                      (take number-of-test-episodes)
                      (map get-predicts))
        rewards (map :reward predicts)
        visited (map :visited predicts)
        rock-counts (frequencies (reduce concat visited))
        travel-counts (reduce
                       (fn [counts rocks]
                         (let [start [(:x instance) (:y instance)]
                               ;; append start node to sequenc of visited rocks
                               us (conj (seq rocks) start)
                               ;; append end node if last rock was not on edge
                               vs (if (= (first (last rocks)) (:n instance))
                                    rocks
                                    (conj (vec rocks)
                                          [(:n instance)
                                           (second (or (last rocks) start))]))]
                           (reduce
                            (fn [counts edge]
                              (update-in counts [edge] (fnil inc 0)))
                            counts
                            (map vector us vs))))
                       {}
                       visited)]
    [rewards rock-counts travel-counts]))

(defn run-trials
  [& {instance-id :instance-id
      number-of-steps-range :number-of-steps-range
      number-of-test-episodes :number-of-test-episodes
      number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {instance-id +instance-id+
           number-of-steps-range +number-of-steps-range+
           number-of-test-episodes +number-of-test-episodes+
           number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}}]
  (doseq [num-steps number-of-steps-range]
    (let [[rewards rock-counts travel-counts policy]
          (trial (instances instance-id)
                 +hed+
                 num-steps
                 number-of-test-episodes
                 :number-of-particles number-of-particles
                 :base-stepsize base-stepsize
                 :adagrad adagrad
                 :robbins-monro robbins-monro)
          file-name (format "results/rockwalk/rockwalk-%s-d%.1f-rho%.1f-gamma%.1f-kappa%.1f-steps%05d-test%d.json"
                            (name instance-id)
                            +hed+
                            base-stepsize
                            adagrad
                            robbins-monro
                            num-steps
                            number-of-test-episodes)]
      (println (format "writing: %s" file-name))
      (spit file-name
            (json/write-str {:rewards rewards
                             :rock-counts (into [] rock-counts)
                             :travel-counts (into [] travel-counts)
                             :policy (into [] policy)
                             :training-parameters {:number-of-particles number-of-particles
                                                   :number-of-steps num-steps
                                                   :base-stepsize base-stepsize
                                                   :adagrad adagrad
                                                   :robbins-monro robbins-monro}
                             :number-of-test-episodes number-of-test-episodes})))))

(defn run-trials-lsmh
  [alg & {:keys [instance-id
                 temperature
                 number-of-test-episodes
                 number-of-mh-iterations]
          :or {instance-id +instance-id+
               temperature +temperature+
               number-of-test-episodes +number-of-test-episodes+
               number-of-mh-iterations +number-of-mh-iterations+
               }
          :as options}]
    (let [[rewards rock-counts travel-counts policy]
          (trial-lsmh alg (instances instance-id)
                 +hed+
                 (if (>= temperature +temperature+)
                   (fn [_] temperature)
                   (let [gamma (/ (log (/ temperature +temperature+))
                                  number-of-mh-iterations)]
                     (fn [iter]
                       (if (< iter number-of-mh-iterations)
                         (* +temperature+ (exp (* gamma iter)))
                         temperature))))
                 number-of-mh-iterations
                 number-of-test-episodes)
          file-name (format "results/rockwalk/rockwalk-%s-%s-d%.1f-t%.3f-test%d.json"
                            (name alg)
                            (name instance-id)
                            +hed+
                            temperature
                            number-of-test-episodes)]
      (println (format "writing: %s" file-name))
      (spit file-name
            (json/write-str {:rewards rewards
                             :rock-counts (into [] rock-counts)
                             :travel-counts (into [] travel-counts)
                             :training-parameters {:number-of-mh-iterations number-of-mh-iterations
                                                   :temperature temperature}
                             :number-of-test-episodes number-of-test-episodes}))))

(def cli-options
  [["-i" "--instance-id NAME" "Instance name"
    :parse-fn keyword
    :default :4x4
    :validate [instances "instance does not exist"]]

   ["-N"
    "--number-of-mh-iterations N"
    "Number of MH iterations (stochastic conditioning)"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-mh-iterations+]

   ["-m" "--number-of-test-episodes M" "Number of test sweeps"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-test-episodes+]

   ["-t" 
    "--temperature T" 
    "Temperature"
    :parse-fn #(Double/parseDouble %)
    :default +temperature+
    :validate [#(> % 0.) "must be positive"]]

   ["-r" "--number-of-steps-range RANGE" "Range of numbers of steps"
    :parse-fn #(read-string (str "[" % "]"))
    :default +number-of-steps-range+]

   ["-a" "--adagrad GAMMA" "Decay rate for AdaGrad/RMSProp"
    :parse-fn #(Double/parseDouble %)
    :default +adagrad+]

   ["-k" "--robbins-monro KAPPA" "Decay rate for step size"
    :parse-fn #(Double/parseDouble %)
    :default +robbins-monro+]

   ["-h" "--help" "Print usage summary and exit"]])

(defn -main
  [& args]
  (let [{:keys [options arguments errors summary]
         :as parsed-options}
        (cli/parse-opts args cli-options)]
    (cond
     (:help options) (println summary)
     errors (println errors)
     (empty? arguments) (println summary)

     :else
     (let [{:keys [instance-id
		   number-of-test-episodes number-of-steps-range
                   adagrad robbins-monro]}
           options]
       (println options)

       (case (first arguments)
         "trials" (apply run-trials (apply concat options))
         "trials-smh" (apply run-trials-lsmh :smh (apply concat options))
         (println summary))))))
