(ns rockwalk.data
  "Problem Instances for rockwalk")

;; Taken from ZMDP (http://longhorizon.org/trey/zmdp/)
(def instances
  "rock values are set to nil and are sampled randomly
  with probability 0.5 to create an instance"
  {:4x4 {:n 4
         :rocks {[3 1] nil [2 1] nil [1 3] nil [1 0] nil}
         :x 0 :y 2}
   :5x5 {:n 5
         :rocks {[2 4] nil [0 4] nil [3 3] nil [2 2] nil [4 1] nil}
         :x 0 :y 2}
   :5x7 {:n 5
         :rocks {[1 0] nil [2 1] nil [1 2] nil [2 2] nil
                 [4 2] nil [0 3] nil [3 4] nil}
         :x 0 :y 2}
   :7x8 {:n 7
         :rocks {[2 0] nil [0 1] nil [3 1] nil [6 3] nil
                 [2 4] nil [3 4] nil [5 5] nil [1 6] nil}
         :x 0 :y 3}
   :10x10 {:n 10
           :rocks {[0 3] nil [0 7] nil [1 8] nil [3 3] nil [3 8] nil
                   [4 3] nil [5 8] nil [6 1] nil [9 3] nil [9 9] nil}
           :x 0 :y 5}})
